const express = require("express");
const router = express.Router();
const Task = require("../models/task");
// const taskTrue = {description: true}
//GET /tasks?completed=true
//GET /tasks?limit=10&skip=0
//GET /tasks?sortBy=createAt_desc
router.get("/tasks", async (req, res) => {
  const match = {};
  try {
    const tasks = await Task.find({});
    res.send({ tasks });
  } catch (error) {
    res.status(500).send(error);
  }
});

router.get("/tasks/:id", async (req, res) => {
  const _id = req.params.id;
  try {
    const task = await Task.findOne({ _id });
    if (!task) {
      return res.status(404).send();
    }
    res.send({ task });
  } catch (error) {
    res.status(500).send(error);
  }
});

router.post("/tasks", async (req, res) => {
  const task = new Task(req.body);
  try {
    await task.save();
    res.status(201).send(task);
  } catch (error) {
    res.status(400).send(error);
  }
});

router.post("/taskCompleted", async (req, res) => {
  // #0 if you use async await should has try/catch
  // #1 validate req.body should contains "description" in JSON if not then return error with status code 400
  // #2 Try to update the completed to "true" with the description
  // const { error } = Task.validate(req.body);
  try {
    if (!req.body.description || !req.body._id)
      throw "กรุณากรอก id หรือ description ที่ต้องการแก้ไข";
    const task = await Task.findOne((id) => {
      id = req.body._id;
    });
    await task.updateOne(
      { description: req.body.description },
      { completed: true }
    );
    const newTask = await Task.findOne((id) => {
      id = req.body._id;
    });
    res.status(200).send(newTask);
  } catch (err) {
    res.status(400).send(err);
  }
});

module.exports = router;
